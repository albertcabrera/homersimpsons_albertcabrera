package com.example.homersimpson_albertcabrera;

import android.graphics.ImageFormat;
import android.graphics.drawable.AnimationDrawable;
import android.media.Image;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    AnimationDrawable titol;
    ImageView image;
    ImageView image2;
    ImageView image3;
    ImageView image4;
    ImageView image5;
    MediaPlayer cancion;
    ImageButton boton;
    int posicion = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView titulo = (ImageView) findViewById(R.id.titulo);
        titulo.setBackgroundResource(R.drawable.animtitulo);
        titol = (AnimationDrawable) titulo.getBackground();
        boton = findViewById(R.id.donut);
        cancion = (MediaPlayer) MediaPlayer.create(this, R.raw.the_simpsons);
        titol.start();


        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (cancion.isPlaying()) {
                   cancion.stop();
               }
                if (cancion != null && cancion.isPlaying() == false) {
                    cancion.seekTo(posicion);
                    cancion.start();
                }
               else{
                   cancion.start();

               }
            }
        });



        RotateAnimation rotate = new RotateAnimation(0, -180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(10000);
        rotate.setInterpolator(new LinearInterpolator());
        image= (ImageView) findViewById(R.id.engVerm);
        image.startAnimation(rotate);

        RotateAnimation rotate2 = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate2.setDuration(10000);
        rotate2.setInterpolator(new LinearInterpolator());
        image2= (ImageView) findViewById(R.id.engverd);
        image2.startAnimation(rotate2);

        RotateAnimation rotate3 = new RotateAnimation(0, -180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate3.setDuration(10000);
        rotate3.setInterpolator(new LinearInterpolator());
        image3= (ImageView) findViewById(R.id.engBlau);
        image3.startAnimation(rotate3);



        RotateAnimation rotate4 = new RotateAnimation(0, -180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate4.setDuration(10000);
        rotate4.setInterpolator(new LinearInterpolator());
        image4= (ImageView) findViewById(R.id.donut);
        image4.startAnimation(rotate4);

        RotateAnimation rotate5 = new RotateAnimation(0, -180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate5.setDuration(10000);
        rotate5.setInterpolator(new LinearInterpolator());
        image5= (ImageView) findViewById(R.id.ull);
        image5.startAnimation(rotate5);



    }

}
